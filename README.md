**Steps to use this project:**

1. **Clone** the project to your machine.
2. **Open you terminal or you can open VScode's terminal**.

	a. **cd** to the project directory.
	
	b. **run yarn install or npm install** to install the project dependencies.
	
	c. **run yarn start or npm start** to start the project on **http://localhost:3000**.
	
3. **Go to your browser** and enter **http://localhost:3000** to test the API.
4. If you want to fetch photos type **http://localhost:3000/photos** in your browser.

**Get Photos:**
	- **http://localhost:3000/photos**

**Post Photo:**

	* make a post request from postman:
		1. Add **http://localhost:3000/photos** at the url area.
		2. Change the requet's method to POST.
		3. Add {"url": "your image url"} to the request body and make sure that the body is of type JSON.
