// import mongoose to connect to the DB and start working with data.
import mongoose from 'mongoose';
// import dotenv to access the screte data saved at the .env file
import * as dotenv from 'dotenv';
// configure the dotenv environment(add .env to process and access saved data)
dotenv.config();

// Connect to mongoose db
mongoose.connect(process.env.DB_CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true }, (error) => {
  if(error){
    console.log(error.message);
  }else {
    console.log("========= Successfully connected to DB =========");
  }
});

// Define the Photo model schema in the DB
const photoSchema = new mongoose.Schema({
  url: {
    type: String,
    required: true,
    default: ""
  }
});

// Add the Photo schema to the photos table(monogo db document)
const Photo = mongoose.model("photos", photoSchema);
export default Photo;