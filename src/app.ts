// import express to enable routing
import express from 'express';
// import the routes for photos model
import photosRouter from './routes/photos';

import {json} from 'body-parser'

// create an instance from express to use it for
// 1- routing
// 2- listening to the server
const app = express();

// Use the json middleware globally to parse the http requests' body.
app.use(json())

// Redirect any /photos route to the photosRouter's routes
app.use("/photos", photosRouter);

// Starting route
app.get("/",(req, res) => {
  return res.send("Hello world");
});

// Listen to the server on port 3000.
app.listen(3000, () => {
  console.log("=========== Server started on port %d ==============", 3000);
});