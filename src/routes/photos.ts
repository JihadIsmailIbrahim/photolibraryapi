import {Router} from 'express';
import Photo from '../models/Photo';

const photosRouter = Router();

// Get all saved photos in ascending order.
photosRouter.get("/", async (req, res) => {
  try {
    const photos = await Photo.find().sort({datefield: -1});
    return res.status(200).json(photos);
  }catch(error) {
    return res.status(400).json({error: error.message});
  }
});

// Create a new photo
photosRouter.post("/", async (req, res) => {
  const photo =  new Photo({url: req.body.url});
  try {
    const savedPhoto = await photo.save()
    return res.status(200).json(savedPhoto);
  }catch(error) {
    return res.status(400).json({error: error.message});
  }
});


export default photosRouter;